#!/bin/sh -xe


# PARAMS: Rust version, OpenSSL version, PostgreSQL version, tag name
RUST_VER=${1:?}
TAG=${2:?}
OPENSSL_VER=${3:-latest}
POSTGRES_VER=${4:-latest}

TEMP_DOCKERFILE="$(mktemp -d)/RustDockerfile"
REPO="registry.gitlab.com/rust_musl_docker/image"

BASE_REPO="registry.gitlab.com/rust_musl_docker/image/base"
BASE_TAG="openssl-${OPENSSL_VER}_postgres-${POSTGRES_VER}"

if [ "$BASE_TAG" = "openssl-latest_postgres-latest" ]; then
	BASE_TAG="latest"
fi

echo "Building Rust dockerfile with Rust ${RUST_VER}, using base image ${BASE_TAG}"

< RustDockerfile.template \
sed "s@BASE_IMAGE@$BASE_REPO:$BASE_TAG@g" | \
sed "s@RUST_TOOLCHAIN@$RUST_VER@g" > "$TEMP_DOCKERFILE"

docker build -f "$TEMP_DOCKERFILE" -t "$REPO":"$TAG" .
docker login registry.gitlab.com -u gitlab-ci-token -p "$CI_JOB_TOKEN"
docker push "$REPO":"$TAG"
